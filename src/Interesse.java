/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Aluno
 */
public class Interesse {
    public int idInteresse;
    public String nomeInteresse;

    public int getIdInteresse() {
        return idInteresse;
    }

    public void setIdInteresse(int idInteresse) {
        this.idInteresse = idInteresse;
    }

    public String getNomeInteresse() {
        return nomeInteresse;
    }

    public void setNomeInteresse(String nomeInteresse) {
        this.nomeInteresse = nomeInteresse;
    }
    
    public int inserirInteresse(Interesse interesse) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO OO_Venda"
                + "(codigoVenda, codigoPessoaFunc, codigoPessoaCli, dataVenda, valorTotal) VALUES"
                + "(seq_venda.nextval,?,?,?,?)";

        try {

            String generatedColumns[] = {"codigoVenda"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setInt(1, venda.getFuncionario().getCodigoPessoa());
            ps.setInt(2, venda.getCliente().getCodigoPessoa());
            ps.setDate(3, venda.gerarVenda());
            ps.setDouble(4, venda.getValorTotal()); //somar valores itens

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            venda.setCodigoVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigoVenda;
    }
   
    
}
